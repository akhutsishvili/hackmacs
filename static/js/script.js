var userID , peerID,lastEntry = 1;
function request(action, data , callback) {
    data[action] = action;
    data.id = window.myID;
    $.ajax({
        dataType: "JSONP",
        type: "GET" ,
        data: data ,
        url: settings.server + "route.php",
        success: function(result) {
            //result = JSON.parse(result);
            callback(result);
        }
    });
} 

function getNow() {
    var time = new Date();
    return time.getHours()+":"+time.getMinutes();
}


function appendMessage(message, me ,time) {
    var cls = "message alert";
    if(me){
        cls += " mine alert-info";
    } else {
        cls += " alert-success";
    }

    var el = $('<div class="'+cls+'">'
               + '<span class="time">'+time+"</span> "
               + "<span class=text>"+message+"</span>"
               + "</div>");

    $(".chat").append(el);
    scrollChat();
    return el;
}
function addMessage(text){
    request("save",
	    {
		"text":text
	    } ,
	    function (h){
		updateChat();
	    });
}

function scrollChat() {
    $(".chat").scrollTop($(".chat").get(0).scrollHeight);
}

function updateChat(){

    var data =
	{
	    "refresh" : "refresh" ,
	};

    $.ajax({
        dataType: "JSONP",
        type: "GET" ,
        data: data ,
        url: settings.server + "route.php",
        success: function(result) {
	    if(lastEntry == result[result.length-1].id){
		return;
	    }
	    else {
		$('.chat').html('');
		for(var r in result){
		    appendMessage(result[r].text,false,result[r].datetime);
		}
	    }
	}
    });
}
setInterval(function(){
    updateChat();
},3000);


$("#form").on('submit',function(event){
    event.preventDefault();
    var text = $('#text').val();
    if(text.length < 1) return;
    $('#text').val("");

    var d = new Date();

    var el = appendMessage(text, true, d.toISOString());

    request("save",
	    {
		"text":text
	    } ,
	    function (result){
		el.addClass("sent");
	    });
    //request("save", { recipient: window.peerID, text: text }, function(result){
        //el.addClass("sent");
    //});
});
