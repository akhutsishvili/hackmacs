<?php
/**
 * @author      Merab Kutalia <kutaliatato@gmail.com>
 * @copyright   2014
 * @todo        Here goes all main requests and routing
 */


// aq mivigebt request ს
require("core/event.php");
$event = new Event();
if(isset($_GET['save'])){
  if(isset($_GET['text'])){
    $message = $_GET['text'];
    $event->addMessage($message);
  }
}
else if(isset($_GET['get'])){
  if(isset($_GET['id'])   && isset($_GET['recipient']) && isset($_GET['entryid'])){
    $user_id = $_GET['id'];
    $recipient_id = $_GET['recipient'];
    $message = $_GET['text'];
    $event->getRow($user_id,$recipient_id,$message);
  }
}
else if(isset($_GET['refresh'])){
    $event->refreshChat();
}

