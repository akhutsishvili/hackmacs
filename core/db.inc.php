<?php
/**
 * @author      Merab Kutalia <kutaliatato@gmail.com>
 * @copyright   2014
 */


class DB {

  public static $db;

  function __construct () {
    $dsn = 'mysql:dbname=sparkinn_macs;host=localhost';
    $user = 'sparkinn_sitest';
    $password = 'root';

    $db = new PDO($dsn, $user, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8") );

    self::$db = $db;
  }
}
$x = new DB();
