<?php
/**
* @author      Merab Kutalia <kutaliatato@gmail.com>
* @copyright   2014
* @todo        Here will be added all common functions
*/

require_once ("db.inc.php");

class CommonFunctions {

    public function get_client_ip()
    {
    	$ipaddress = '';
    	if($_SERVER['REMOTE_ADDR'])
    		$ipaddress = $_SERVER['REMOTE_ADDR'];
    	else
    		$ipaddress = 'UNKNOWN';

    	return $ipaddress;
    }

    public function error($error = null, $reason = null) // შეცდომის ლოგირება
    {
        $file = 'log/php.log';
        $ip = $this->get_client_ip();
        if ($error != null) {
            $query = DB::$db->prepare('INSERT INTO log (ip,reason) VALUES (:ip,:reason)');
            $result = $query->execute(array(':ip' => $ip,':reason'=>$reason));
            if (!$result) {         // თუ ბაზა დაწვა გადავიდვართ ფაილში ლოგირებაზე.
                if (!file_exists(dirname($file))) {
                    mkdir(dirname($file), 0777, true);
                }
                error_log($error . date('D, d M Y H:i:s T') . ' IP: ' . $ip . " \r\n", 3, $file);
            }
        } else {
            error_log('undefined error' . date('D, d M Y H:i:s T') . ' IP: ' . $ip . " \r\n", 3, $file);
        }
    }

// function loadJSON($data){   //json parser
//     $jsonArray = json_decode($data, true);
//     foreach ($jsonArray as $key) {
//         if(is_array($key)){
//             $name = sanitizeMySQL($key['name']);
//             $last = sanitizeMySQL($key['last_name']);
//             $age = sanitizeMySQL($key['age']);
//             $sex = sanitizeMySQL($key['sex']);

//             insertEvent($name,$last,$age,$sex);
//         }
//     }
// }  // sxva klashi iqneba gadasatani da gadasaketebeli, nu daaxloebit es unda iyos

}