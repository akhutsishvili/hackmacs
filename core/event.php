<?php

require_once('commonFunctions.php');

class Event {
  private $commonFunctions;
  function __construct(){
    $this->commonFunctions = new CommonFunctions();
  }
  public function addMessage($message){
    DB::$db->beginTransaction();
    try {
      $query = DB::$db->prepare('INSERT INTO chat (`text`) VALUES (:text)');
      $result = $query->execute(array(':text'=>$message ));
      DB::$db->commit();
      //echo json_encode('შეინახა', JSON_UNESCAPED_UNICODE);
      
      
    } catch (PDOException $e) {
      DB::$db->rollback();
      $this->commonFunctions->error('Not Correct Query ');
    }
  }


  public function refreshChat(){
    $query = DB::$db->prepare("
			        SELECT * FROM `chat` ORDER BY `id` DESC LIMIT 30
				");
    $result = $query->execute();
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    if($result){
      //die(print_r($result));
      $result = array_reverse($result);
      echo $_GET['callback']."(".json_encode($result).")";
    }
    else{
      $this->commonFunctions->error('Not Correct Query ');
    }
}

} // end class

